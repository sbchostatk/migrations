package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.AccountService;
import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.EmploymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employment/records")
public class EmploymentController {
    @Autowired
    private EmploymentService employmentService;

    @PostMapping(value = "/update/{person_id}")
    public ResponseEntity<?> update(@PathVariable Long person_id,
                                    @RequestBody @Validated List<EmploymentDTO> employmentsDTO) {
        employmentService.update(employmentsDTO, person_id);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
