package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.Employment;
import org.springframework.stereotype.Service;

import java.util.List;


public interface EmploymentService {
    void saveEmployee(EmploymentDTO employmentDTO);

    List<EmploymentDTO> getListEmployments();

    void update(List<EmploymentDTO> employmentDTO, Long person_id);
}
