package com.example.exampleliqubase.dto;

import com.sun.istack.internal.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {
    private Long employment_id;

    private int version;

    private Date start_dt;

    private Date end_dt;

    private Long work_type_id;

    private String organization_name;

    private String organization_address;

    private String position_name;

    private Long person_id;


}
