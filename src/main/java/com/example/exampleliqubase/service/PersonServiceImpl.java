package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.PersonService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.dto.PersonDTO;
import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.model.Person;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void init() {
        System.out.println();
    }

    @Override
    public void save(PersonDTO personDTO) {
        Person person = modelMapper.map(personDTO, Person.class);
        personMapper.insert(person);
    }
}
