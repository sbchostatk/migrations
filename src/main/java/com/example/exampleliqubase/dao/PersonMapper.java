package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.model.Person;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface PersonMapper {
    @SelectKey(resultType = Long.class, keyProperty = "person_id", before = true,
            statement = "select nextval('person_seq')")
    @Insert("insert into person (person_id, first_name, last_name, middle_name, birth_date, gender) " +
            "values (#{person_id}, #{first_name} ,#{last_name} ,#{middle_name},#{birth_date},#{gender})")
    void insert(Person person);

    @Select("select * from person")
    List<Person> getListPerson();
}
