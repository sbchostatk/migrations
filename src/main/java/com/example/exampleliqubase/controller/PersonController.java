package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.PersonService;
import com.example.exampleliqubase.dto.PersonDTO;
import com.example.exampleliqubase.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    @PostMapping("/save")
    public ResponseEntity<?> savePerson(@RequestBody @Validated PersonDTO personDTO) {
        personService.save(personDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
