package com.example.exampleliqubase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Employment {
    private Long employment_id;
    private int version;
    private Date start_dt;
    private Date end_dt;
    private Long work_type_id;
    private String organization_name;
    private String organization_address;
    private String position_name;
    private Long person_id;

    public Long getEmployment_id() {
        return employment_id;
    }
}
