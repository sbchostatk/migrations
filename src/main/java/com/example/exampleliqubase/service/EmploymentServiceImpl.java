package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.Employment;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    @Autowired
    private EmploymentMapper employmentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    public void init() {
        System.out.println();
    }

    @Override
    public void saveEmployee(EmploymentDTO employmentDTO) {
        Employment employment = modelMapper.map(employmentDTO, Employment.class);
        employmentMapper.save(employment);
    }

    @Override
    public List<EmploymentDTO> getListEmployments() {
        return null;
    }

    @Override
    public void update(List<EmploymentDTO> employmentDTO, Long person_id) {
        List<Employment> employmentEntities = employmentMapper.getByPersonId(person_id);

        List<Employment> save = new ArrayList<>();
        List<Employment> update = new ArrayList<>();

        for (EmploymentDTO dto : employmentDTO) {
            Employment newEmploymentEntity = modelMapper.map(dto, Employment.class);
            Long employmentId = newEmploymentEntity.getEmployment_id();

            if (employmentId != null) {
                for (Employment emp: employmentEntities) {
                    if (emp.getEmployment_id().equals(employmentId)) {
                        if (!emp.equals(newEmploymentEntity)) {
                            update.add(newEmploymentEntity);
                            employmentEntities.remove(emp);
                            break;
                        }
                    }
                }
            }
            else
                save.add(newEmploymentEntity);
        }

        if (!save.isEmpty())
            employmentMapper.insertAll(save);

        if(!employmentEntities.isEmpty()) {
            for (Employment e : employmentEntities) {
                employmentMapper.deleteById(e.getEmployment_id());
            }
        }

        if(!update.isEmpty())
            employmentMapper.updateAll(update);
    }
}
