package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.AccountEntity;
import com.example.exampleliqubase.model.Employment;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EmploymentMapper {
    @SelectKey(resultType = Long.class, keyProperty = "employment_id", before = true,
            statement = "select nextval('employment_seq')")
    @Insert("insert into employment (employment_id, version, start_dt, end_dt, work_type_id, organization_name, organization_address, position_name, person_id) " +
            "values (#{employment_id}, #{version} ,#{start_dt} ,#{end_dt},#{work_type_id},#{organization_name},#{organization_address},#{position_name},#{person_id} )")
    void save(Employment employment);

    @Select("select * from employment")
    List<Employment> getListEmployments();

    @Select("select * from employment where id=#{id}")
    List<Employment> findById(Long id);

    //@Select("select * from employment where person_id=#{person_id}")
    List<Employment> getByPersonId(Long person_id);

    void insertAll(@Param("list") List<Employment> save);

    @Delete("delete from employment where employment_id=#{id}")
    void deleteById(Long id);

    void updateAll(@Param("list") List<Employment> update);
}
